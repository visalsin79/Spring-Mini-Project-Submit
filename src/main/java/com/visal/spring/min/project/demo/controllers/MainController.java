package com.visal.spring.min.project.demo.controllers;

import com.visal.spring.min.project.demo.models.Book;
import com.visal.spring.min.project.demo.models.Category;
import com.visal.spring.min.project.demo.services.BookService;
import com.visal.spring.min.project.demo.services.CategoryService;
import com.visal.spring.min.project.demo.services.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Controller
public class MainController {
    private BookService bookService;
    private CategoryService categoryService;
    private UploadService uploadService;
    @Autowired
    public MainController(BookService bookService, UploadService uploadService, CategoryService categoryService) {
        this.bookService = bookService;
        this.uploadService = uploadService;
        this.categoryService = categoryService;
    }
    @RequestMapping({"/index", "/"})
    public String dashboard(Model model) {
        Integer countBook = this.bookService.count();
        Integer countCategory = this.categoryService.count();
        model.addAttribute("count_book", countBook);
        model.addAttribute("date", new Date());
        model.addAttribute("count_category", countCategory);

        return "index";
    }

    @GetMapping("/book")
    public String allBook(ModelMap model) {
        Integer countBook = this.bookService.count();
        Integer countCategory = this.categoryService.count();
        List<Book> bookList = this.bookService.getAll();
        model.addAttribute("books", bookList);
        model.addAttribute("count_book", countBook);
        model.addAttribute("count_category", countCategory);
        return "book/all-book";
    }


    @RequestMapping(method = RequestMethod.GET, value = {"/all-category"})
    public String allCategory(ModelMap model) {
        List<Category> categoriesList = this.categoryService.getAll();
        model.addAttribute("books", categoriesList);
        return "book/all-category";
    }


    @GetMapping("/view/{id}")
    public String viewDetail(@PathVariable("id") Integer id, Model model) {
        System.out.println("ID: " + id);
        Book book = this.bookService.findOne(id);
        model.addAttribute("book", book);
        return "book/view-detail";
    }


    @GetMapping("/update/{id}")
    public String showUpdateForm(@PathVariable Integer id, ModelMap modelMap) {
        Book book = this.bookService.findOne(id);
        modelMap.addAttribute("isNew", false);
        modelMap.addAttribute("book", book);
        List<Category> categories = this.categoryService.getAll();
        modelMap.addAttribute("categories", categories);

        return "book/create-book";
    }




    @GetMapping("/remove-category/{id}")
    public String removeCate(@PathVariable Integer id) {
        System.out.println(id);
        this.categoryService.remove_category(id);

        return "redirect:/all-category";
    }





    @GetMapping("remove/{id}")
    public String remove(@PathVariable Integer id) {
        this.bookService.remove(id);

        return "redirect:/book";
    }


    @GetMapping("/count")
    @ResponseBody
    public Map<String, Object> count() {
        Map<String, Object> response = new HashMap<>();

        response.put("record_count", this.bookService.count());
        response.put("status", true);

        return response;
    }


    @GetMapping("/create")
    public String create(Model model) {

        model.addAttribute("book", new Book());
        model.addAttribute("isNew", true);

        List<Category> categories = this.categoryService.getAll();
        model.addAttribute("categories", categories);

        System.out.println(categories);

        return "book/create-book";
    }


    @PostMapping("/create/submit")
    public String createSubmit(@Valid Book book, BindingResult bindingResult, MultipartFile file,Model model) {
        System.out.println(book);
        if(book.getCategory().equals(null)){
            return "redirect:/book";
        }
        if (bindingResult.hasErrors()) {
            List<Category> categories = this.categoryService.getAll();
            model.addAttribute("book", book);
            model.addAttribute("categories", categories);
            model.addAttribute("isNew", true);
            return "book/create-book";
        }
        String filename = this.uploadService.upload(file, "pp/");
        book.setThumbnail("/images/" + filename);
        this.bookService.create(book);
        return "redirect:/book";
    }

    @PostMapping("/update/submit")
    public String updateSubmit(@Valid Book book, BindingResult bindingResult, MultipartFile file,Model model) {

        if (bindingResult.hasErrors()) {

            model.addAttribute("isNew", false);
            model.addAttribute("book", book);
            List<Category> categories = this.categoryService.getAll();
            model.addAttribute("categories", categories);
            return "book/create-book";
        }

        File path = new File("/ppbook");

        if (!path.exists())
            path.mkdir();
        String filename = file.getOriginalFilename();
        String extension = filename.substring(filename.lastIndexOf('.') + 1);
        filename = UUID.randomUUID() + "." + extension;
        try {
            Files.copy(file.getInputStream(), Paths.get("/ppbook", filename));
        } catch (IOException e) {

        }
        if (!file.isEmpty()) {
            book.setThumbnail("/images/" + filename);
        }
      this.bookService.update(book);
        return "redirect:/book";
    }



    @GetMapping("/add-test")
    public String create_category(Model model) {

        model.addAttribute("book", new Category());

        return "book/create-category";
    }


    @PostMapping("/category/submit")
    public String createSubmit(@Valid Category category,BindingResult bindingResult,Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("book", new Category());
            return "book/create-category";
        }
        this.categoryService.create_category(category);

        return "redirect:/all-category";
    }

    @GetMapping("/update-category/{id}")
    public String updateCategory(@PathVariable Integer id, ModelMap modelMap) {


        Category category = this.categoryService.findOne(id);

        modelMap.addAttribute("book", category);
        return "book/update-category";
    }


    @PostMapping("/update-category/submit")
    public String updateSubmit(@Valid Category category,BindingResult bindingResult,Model model) {

        if (bindingResult.hasErrors()) {

            model.addAttribute("book", category);
            return "book/create-category";
        }
        this.categoryService.update_category(category);
        return "redirect:/all-category";
    }


}
