package com.visal.spring.min.project.demo.models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class Category {

    private Integer id;
    @NotNull
    @Size(min = 2, max = 50)
    private String name;

    public Category() {
    }
    public Category(Integer id, @NotNull @Size(min = 2, max = 50) String name) {
        this.id = id;
        this.name = name;

    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
