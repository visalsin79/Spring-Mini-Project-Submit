package com.visal.spring.min.project.demo.services;



import com.visal.spring.min.project.demo.models.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category findOne(Integer id);

    Integer count();

    boolean create_category(Category category);

    boolean update_category(Category category);

    boolean remove_category(Integer id);

}
