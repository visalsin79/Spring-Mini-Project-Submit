package com.visal.spring.min.project.demo.services;


import com.visal.spring.min.project.demo.models.Book;

import java.util.List;

public interface BookService {

    List<Book> getAll();

    Book findOne(Integer id);

    boolean remove(Integer id);

    Integer count();

    boolean create(Book book);

    boolean update(Book book);

}
